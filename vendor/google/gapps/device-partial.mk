#
# Copyright 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# GApps dependencies
PRODUCT_COPY_FILES += \
	vendor/google/gapps/proprietary/system/lib/libjni_latinimegoogle.so:system/lib/libjni_latinimegoogle.so:google \
	vendor/google/gapps/proprietary/system/priv-app/GMSCore/lib/arm/libWhisper.so:system/priv-app/GMSCore/lib/arm/libWhisper.so:google \
	vendor/google/gapps/proprietary/system/priv-app/GMSCore/lib/arm/libgmscore.so:system/priv-app/GMSCore/lib/arm/libgmscore.so:google \
	vendor/google/gapps/proprietary/system/priv-app/GMSCore/lib/arm/libwearable-selector.so:system/priv-app/GMSCore/lib/arm/libwearable-selector.so:google \
	vendor/google/gapps/proprietary/system/priv-app/GMSCore/lib/arm/libAppDataSearch.so:system/priv-app/GMSCore/lib/arm/libAppDataSearch.so:google \
	vendor/google/gapps/proprietary/system/priv-app/GMSCore/lib/arm/libgms-ocrclient.so:system/priv-app/GMSCore/lib/arm/libgms-ocrclient.so:google \
	vendor/google/gapps/proprietary/system/priv-app/GMSCore/lib/arm/libconscrypt_gmscore_jni.so:system/priv-app/GMSCore/lib/arm/libconscrypt_gmscore_jni.so:google \
	vendor/google/gapps/proprietary/system/priv-app/GMSCore/lib/arm/libNearbyApp.so:system/priv-app/GMSCore/lib/arm/libNearbyApp.so:google \
	vendor/google/gapps/proprietary/system/priv-app/GMSCore/lib/arm/libleveldbjni.so:system/priv-app/GMSCore/lib/arm/libleveldbjni.so:google \
	vendor/google/gapps/proprietary/system/priv-app/GMSCore/lib/arm/libsslwrapper_jni.so:system/priv-app/GMSCore/lib/arm/libsslwrapper_jni.so:google \
	vendor/google/gapps/proprietary/system/priv-app/GMSCore/lib/arm/libappstreaming_jni.so:system/priv-app/GMSCore/lib/arm/libappstreaming_jni.so:google \
	vendor/google/gapps/proprietary/system/priv-app/GMSCore/lib/arm/libjgcastservice.so:system/priv-app/GMSCore/lib/arm/libjgcastservice.so:google \
	vendor/google/gapps/proprietary/system/priv-app/GMSCore/lib/arm/libgcastv2_base.so:system/priv-app/GMSCore/lib/arm/libgcastv2_base.so:google \
	vendor/google/gapps/proprietary/system/priv-app/GMSCore/lib/arm/libdirect-audio.so:system/priv-app/GMSCore/lib/arm/libdirect-audio.so:google \
	vendor/google/gapps/proprietary/system/priv-app/GMSCore/lib/arm/libgcastv2_support.so:system/priv-app/GMSCore/lib/arm/libgcastv2_support.so:google \
	vendor/google/gapps/proprietary/system/etc/permissions/com.google.android.maps.xml:system/etc/permissions/com.google.android.maps.xml:google \
	vendor/google/gapps/proprietary/system/etc/permissions/com.google.android.camera2.xml:system/etc/permissions/com.google.android.camera2.xml:google \
	vendor/google/gapps/proprietary/system/etc/permissions/com.google.widevine.software.drm.xml:system/etc/permissions/com.google.widevine.software.drm.xml:google \
	vendor/google/gapps/proprietary/system/etc/permissions/com.google.android.media.effects.xml:system/etc/permissions/com.google.android.media.effects.xml:google \
	vendor/google/gapps/proprietary/system/etc/permissions/com.google.android.dialer.support.xml:system/etc/permissions/com.google.android.dialer.support.xml:google \
	vendor/google/gapps/proprietary/system/framework/com.google.android.camera2.jar:system/framework/com.google.android.camera2.jar:google \
	vendor/google/gapps/proprietary/system/framework/com.google.widevine.software.drm.jar:system/framework/com.google.widevine.software.drm.jar:google \
	vendor/google/gapps/proprietary/system/framework/com.google.android.dialer.support.jar:system/framework/com.google.android.dialer.support.jar:google \
	vendor/google/gapps/proprietary/system/framework/com.google.android.maps.jar:system/framework/com.google.android.maps.jar:google \
	vendor/google/gapps/proprietary/system/framework/com.google.android.media.effects.jar:system/framework/com.google.android.media.effects.jar:google

PRODUCT_PACKAGES += \
	GoogleOneTimeInitializer \
	BackupRestoreConfirmation_GApps \
	Phonesky \
	GoogleBackupTransport \
	GMSCore \
	GoogleServicesFramework \
	GoogleFeedback \
	GooglePartnerSetup \
	GoogleLoginService \
	ConfigUpdater \
	GoogleCalendarSyncAdapter \
	ChromeBookmarksSyncAdapter \
	GoogleContactsSyncAdapter

